# Client Serveur - TP1

## 💡 Introduction

**Récupération des samples**

```sh
curl https://github.com/neelabalan/mongodb-sample-dataset/blob/main/sample_mflix/movies.json --output movies.json
curl https://github.com/neelabalan/mongodb-sample-dataset/blob/main/sample_mflix/comments.json --output comments.json
```

**Création de la base de données**

```
use tp1
```

**Création des collections**

```
db.createCollection("movies");
db.createCollection("comments");
```

**Import des fichiers JSON**

```
mongoimport --collection movies movies.json --db tp1
mongoimport --collection comments comments.json --db tp1
```

## 💻 Queries

### 1 - Compter le nombre de Thriller présents en base (films qui possèdent au moins le genre Thriller)

```js
db.movies.countDocuments({
  genres: "Thriller"
});
```

ou

```js
db.movies.aggregate([
  { $match: { genres: "Thriller" } },
  { $count: "Number of mvovies contains Thriller in genres" }
]);
```

ou 

```js
db.movies.find({
  genres: "Thriller"
}).count();
```

### 2 - Trouver la liste des films qui contiennent le mot "ghost" dans leur titre 

#### Case sensitive

```js
db.movies.find({ 
  title: /.*ghost.*/
});
```

#### Case insensitive

```js
db.movies.find({ 
  title: /.*ghost.*/i
});
```

### 3 - Trouver la liste des films qui contiennent le mot "ghost" dans leur titre et qui sont sortis après 2013 

#### Case sensitive

```js
db.movies.find({
  title: /.*ghost.*/,
  year: { $gt: 2013 },
});
```

#### Case insensitive

```js
db.movies.find({
  title: /.*ghost.*/i,
  year: { $gt: 2013 },
});
```

### 4 - Trouver le film qui a gagné le plus de récompenses 

```js
db.movies.aggregate([
{
  $match: { "awards.wins": { $exists: true } }
},
{
  $sort: { "awards.wins": -1 }
},
{
  $limit: 1
}
]);
```

ou 

```js
db.movies.find({
  "awards.wins": { $exists: true }
}).sort({ 
  "awards.wins": -1
}).limit(1);
```

### 5 - Trouver le plus vieux film de plus de 2 heures ayant une note inférieur à 2 sur la plateforme imdb 

```js
db.movies.aggregate([
{
  $match: {
    "runtime": { $exists: true },
    "imdb.rating": { $exists: true },
    "year": { $exists: true },
  }
},
{
  $match: {
    "runtime": { $gt: 120 },
    "imdb.rating": { $lt: 2 },
  }
},
{
  $sort: { "year": -1 }
},
{
  $limit: 1
}
]);
```

### 6 - Modifier la requête précédente pour récupérer en même temps les commentaires. 

```js
db.movies.aggregate([
{
  $match: {
    "runtime": { $exists: true },
    "imdb.rating": { $exists: true },
    "year": { $exists: true },
  }
},
{
  $match: {
    "runtime": { $gt: 120 },
    "imdb.rating": { $lt: 2 },
  }
},
{
  $sort: { "year": -1 }
},
{
  $limit: 1
},
{
  $lookup: {
    from: "comments",
    localField: "_id",
    foreignField: "movie_id",
    as: "movie_comments"
  }
}
]);
```

### 7 - Récupérer le nombre de films par pays ainsi que leurs titres.

```js
db.movies.aggregate([
{
  $match: {
    "countries": { $exists: true }
  }
},
{
  $unwind: "$countries"
},
{
  $group: {
    _id: "$countries",
    count: { $sum: 1 }
  }
},
{
  $sort: { "count": -1 }
}
]);
```

## 👨‍💻 Author 

**BREUIL Yohann**

* GitHub: [@DJYohann](https://github.com/DJYohann)
* LinkedIn: [@BREUIL Yohann](https://www.linkedin.com/in/yohann-breuil-02b18a165/)